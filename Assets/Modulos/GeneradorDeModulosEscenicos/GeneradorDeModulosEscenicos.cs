﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorDeModulosEscenicos : MonoBehaviour {
	private PoolObjects po;
	public Transform target;
	public List<int> TodosLosModulos; // ID en la lista de PoolObjects
	public List<int> instanciados;
	public float DistanciaEntreInstancias = 5f;
	public Transform PisoTecho;
	public Transform paredDeRetroceso;

	// Use this for initialization
	void Start () {
		po = FindObjectOfType<PoolObjects>();
		Instanciar();
		Instanciar();
		Instanciar();
		Instanciar();
		Instanciar();
		Instanciar();
		Instanciar();
		InvokeRepeating("miUpdate", 0.2f, 0.2f);
	}

	void miUpdate(){
		float recorrido = Vector3.Distance(transform.position, target.position);

		if(recorrido >= (instanciados.Count-1)*DistanciaEntreInstancias){
			Instanciar();
		}


	}

	public void Instanciar(){
		int instanInt = TodosLosModulos[Random.Range(0, TodosLosModulos.Count)];
		instanciados.Add(instanInt);
		po.Intanciar(instanInt, transform.position + transform.forward*instanciados.Count*DistanciaEntreInstancias, Quaternion.Euler(-90f,0f,180f));
		PisoTecho.localScale = PisoTecho.localScale + new Vector3(0,0,instanciados.Count);

		if(instanciados.Count > 15){
			paredDeRetroceso.position = transform.position + transform.forward*(instanciados.Count-15)*DistanciaEntreInstancias;
		}
	}
}
