﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuloEscenico : MonoBehaviour {
	public Transform instanciador;
	private PoolObjects po;
	public List<int> instanciasPosibles;
	private GeneradorDeModulosEscenicos gdme;

	// Use this for initialization
	void Start () {
		po = FindObjectOfType<PoolObjects>();
		gdme = FindObjectOfType<GeneradorDeModulosEscenicos>();
		InvokeRepeating("miUpdate", 1f, 1f);
		if(instanciasPosibles.Count > 0){
			Instanciar();
		}
	}

	void OnEnable(){
		if(instanciasPosibles.Count > 0){
			Instanciar();
		}
	}

	void miUpdate(){
		if(gdme.target){
			if(Vector3.Distance(transform.position, gdme.target.position) > gdme.DistanciaEntreInstancias*15){
				po.Destruir(this.gameObject);
			}
		}
	}

	public void Destruir(){
		po.Destruir(this.gameObject);
	}

	void Instanciar(){
		int id = instanciasPosibles[Random.Range(0, instanciasPosibles.Count)];
		po.Intanciar(id, instanciador.position, instanciador.rotation);
	}
}
