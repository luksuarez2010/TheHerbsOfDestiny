﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CargarEscenaNueva : MonoBehaviour {
	public int EscenaID = 1;
	public void LoadScene(){
		SceneManager.LoadScene(EscenaID);
	}
}
