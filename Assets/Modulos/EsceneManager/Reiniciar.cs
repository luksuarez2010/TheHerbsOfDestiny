﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Reiniciar : MonoBehaviour {

	void Update(){
		if(Input.GetKeyUp(KeyCode.Space) || Input.GetKeyUp(KeyCode.Escape)){
			ReiniciarEscena();
		}
	}

	public void ReiniciarEscena(){
		SceneManager.LoadScene(0);
	}
}
