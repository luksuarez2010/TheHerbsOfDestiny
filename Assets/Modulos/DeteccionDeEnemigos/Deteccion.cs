﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class Deteccion : MonoBehaviour {
	public float distancia = 7f;
	public List<Detectado> targets;
	public Detectado target;
	public int tiempoDeEsperaParaVolverAAtacar = 10;
	private int tiempoEsperando;
	public UnityEvent Atacar;

	public PoolObjects po;

	// Use this for initialization
	void Start () {
		targets.AddRange(FindObjectsOfType<Detectado>());
		InvokeRepeating("miUpdate", 1f, 1f);
		po = FindObjectOfType<PoolObjects>();
	}
	
	// Update is called once per frame
	void miUpdate () {
		if(tiempoEsperando == 0){
			foreach(Detectado det in targets){
				float dist = Vector3.Distance(transform.position, det.transform.position);
				if(dist <= distancia){
					target = det;
					Atacar.Invoke();
				}else{
					target = null;
				}
			}
		}
		if(target){
			transform.LookAt(new Vector3(target.transform.position.x, transform.position.y, transform.position.z));
			tiempoEsperando ++;
			if(tiempoEsperando >= tiempoDeEsperaParaVolverAAtacar) tiempoEsperando = 0;
		}
	}
	public void Dificultar(){
		if(tiempoDeEsperaParaVolverAAtacar > 2)
			tiempoDeEsperaParaVolverAAtacar --;
	}
}
