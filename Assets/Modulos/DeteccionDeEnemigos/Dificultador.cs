﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dificultador : MonoBehaviour {
	public List<Deteccion> detectores;

	public void Dificultar(){
		print("Dificultando");
		detectores.Clear();
		detectores.AddRange(FindObjectsOfType<Deteccion>());
		StartCoroutine(dificultarEnemigo());
	}

	IEnumerator dificultarEnemigo(){
		yield return new WaitForSeconds(0);
		foreach(Deteccion det in detectores){
			det.Dificultar();
		}
	}
}
