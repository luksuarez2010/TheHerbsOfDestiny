﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;

public class Vida : MonoBehaviour {
	public int vida = 10;
	public string tagDanio = "Respawn";
	public UnityEvent muerte, mensajeDeDanio;
	public Text vidaUI;

	// Use this for initialization
	void OnCollisionEnter (Collision col) {
		if(col.transform.tag == tagDanio){
			vida--;
			mensajeDeDanio.Invoke();
			if(vida <= 0){
				muerte.Invoke();
			}
			vidaUI.text = vida.ToString();
		}
	}
}
