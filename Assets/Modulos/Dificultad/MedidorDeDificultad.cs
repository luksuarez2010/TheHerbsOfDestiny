﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MedidorDeDificultad : MonoBehaviour {
	public Text DificultadUI;
	public int dificultad;

	public void SumaDificultad(int dif){
		dificultad += dif;
		DificultadUI.text = dificultad.ToString();
	}
}
