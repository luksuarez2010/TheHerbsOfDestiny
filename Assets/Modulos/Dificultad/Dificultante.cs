﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dificultante : MonoBehaviour {
	private MedidorDeDificultad mdd;
	private Dificultador difUI;
	public int dificultad;

	// Use this for initialization
	void Start () {
		mdd = FindObjectOfType<MedidorDeDificultad>();
		difUI = FindObjectOfType<Dificultador>();
	}
	public void sumarDificultad(){
		mdd.SumaDificultad(dificultad);
		difUI.Dificultar();
	}
}
