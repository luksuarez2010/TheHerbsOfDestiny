﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine;

public class PuntosAcumulados : MonoBehaviour {
	public int puntaje;
	public Text puntajeUI;
	public UnityEvent evento;


	public void SumaPuntos(int suma){
		puntaje += suma;
		puntajeUI.text = puntaje.ToString();
		evento.Invoke();
	}
}
