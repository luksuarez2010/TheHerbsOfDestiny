﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuntosIndividuo : MonoBehaviour {
	public int puntos;
	private PuntosAcumulados pa;

	void Start () {
		pa = FindObjectOfType<PuntosAcumulados>();
	}
	
	public void sumar(){
		pa.SumaPuntos(puntos);
	}
}
