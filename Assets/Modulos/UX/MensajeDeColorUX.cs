﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class MensajeDeColorUX : MonoBehaviour {
	public Image miUX;
	private Color miColorStart;
	public Color colorDanio, colorBien;
	public float velTrans;

	// Use this for initialization
	void Start () {
		miColorStart = miUX.color;
	}
	
	// Update is called once per frame
	void Update () {
		if(miUX.color != miColorStart){
			miUX.color = Vector4.Lerp(miUX.color, miColorStart, Time.deltaTime * velTrans);
		}
	}

	public void DanioUX(){
		miUX.color = colorDanio;
	}
	public void BienUX(){
		miUX.color = colorBien;
	}
}
