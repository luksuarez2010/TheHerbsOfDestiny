﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispararSonido2D : MonoBehaviour {
	private AudioSourceGeneral2D miSFX;
	public AudioClip miclip;

	// Use this for initialization
	void Start () {
		miSFX = FindObjectOfType<AudioSourceGeneral2D>();
	}
	
	// Update is called once per frame
	public void PlayAudio() {
		miSFX.PlayAudio(miclip);
	}
}
