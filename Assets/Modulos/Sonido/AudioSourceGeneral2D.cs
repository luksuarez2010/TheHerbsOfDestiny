﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSourceGeneral2D : MonoBehaviour {
	private AudioSource misource;
	// Use this for initialization
	void Start () {
		misource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	public void PlayAudio(AudioClip Clip) {
		misource.PlayOneShot(Clip);
	}
}
