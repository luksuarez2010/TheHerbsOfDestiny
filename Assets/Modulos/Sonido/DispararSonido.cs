﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DispararSonido : MonoBehaviour {
	private AudioSource audioSource;
	public AudioClip audioClip;
	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
	}
	
	public void PlayAudio(){
		audioSource.PlayOneShot(audioClip);
	}
}
