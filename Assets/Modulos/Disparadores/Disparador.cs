﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class Disparador : MonoBehaviour {
	private PoolObjects po;
	public Transform disparador;
	public float desvioDeDisparos = 20f, velMetra = 0.5f;
	private Quaternion rotDispStart;
	public int cantMetra = 7;
	public UnityEvent cuandoDispara;

	// Use this for initialization
	void Start () {
		po = FindObjectOfType<PoolObjects>();
		rotDispStart = disparador.localRotation;
	}

	public void DisparoMetra(){
		if(gameObject.activeSelf)
			StartCoroutine(disparoMetra());
	}

	IEnumerator disparoMetra(){
		for(int d = 0; d < cantMetra; d++){
			Disparar();
			yield return new WaitForSeconds (velMetra);
		}
	}

	public void Disparar(){
		StartCoroutine(disparar());
	}
	IEnumerator disparar(){
		yield return new WaitForSeconds(0);
		disparador.localRotation = rotDispStart * Quaternion.Euler(new Vector3(Random.Range(0, desvioDeDisparos), 0f, 0f));
		yield return new WaitForSeconds(0.01f);
		po.Intanciar(0, disparador.position, disparador.rotation);
		cuandoDispara.Invoke();
	}
}
