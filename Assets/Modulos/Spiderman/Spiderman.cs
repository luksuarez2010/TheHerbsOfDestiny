﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spiderman : MonoBehaviour {
	public SpringJoint joint;
	public Telarania Enganche;
	public List<Rigidbody> dobleses;
	public Transform disparador;
	public float velocidadDeDisparo = 50f;
	public Vector3 mousePosition;
	private float springValue;
	public LineRenderer cadena;

	// Use this for initialization
	void Start () {
		springValue = joint.spring;
		Enganche.miSpider = this;
	}

	void Update () {
		if(Input.GetMouseButtonDown(1)){
			LanzaEnganche();
		}

		mousePosition = Camera.main.ScreenPointToRay(Input.mousePosition).direction * Vector3.Distance(Camera.main.transform.position, disparador.transform.position);
		disparador.LookAt(new Vector3(mousePosition.x+Camera.main.transform.position.x, mousePosition.y, disparador.position.z));

		transform.LookAt(new Vector3(Enganche.transform.position.x, transform.position.y, transform.position.z));


		cadena.SetPosition(0, transform.position);
		cadena.SetPosition(1, Enganche.transform.position);

		if(Enganche.disparado){
			joint.spring = 0f;
		}else{
			joint.spring = springValue;
		}
	}

	public void LanzaEnganche(){
		Enganche.transform.position = disparador.position;
		Enganche.transform.rotation = disparador.rotation;
		Enganche.SetVel(velocidadDeDisparo);
		Enganche.disparado = true;
		joint.spring = springValue;
	}
}
