﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Telarania : MonoBehaviour {
	public bool disparado;
	[HideInInspector]
	public Rigidbody mirigid;
	private float velocidad;

	[HideInInspector]
	public Spiderman miSpider;

	// Use this for initialization
	public void SetVel (float vel) {
		velocidad = vel;
	}
	
	// Update is called once per frame
	void Update () {
		if(disparado){
			transform.position += transform.forward * velocidad * Time.deltaTime;
		}
		if(colContact && !colContact.gameObject.activeSelf){
			colContact = null;
			disparado = true;
		}
	}

	public Collider colContact;
	void OnCollisionEnter(Collision col){
		disparado = false;
		colContact = col.collider;
	}
}
