﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotacionConstante : MonoBehaviour {
	public Vector3 rot;
	public float vel;
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.Lerp(transform.rotation, transform.rotation * Quaternion.Euler(rot),  Time.deltaTime * vel);
	}
}
