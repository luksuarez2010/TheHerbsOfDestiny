﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollowChar : MonoBehaviour {
	public Transform target;
	public float vel = 6f;
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.Lerp(transform.position, new Vector3(target.position.x, transform.position.y, transform.position.z), Time.deltaTime * vel);
	}
}
