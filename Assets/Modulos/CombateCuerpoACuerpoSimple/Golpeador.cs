﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class Golpeador : MonoBehaviour {
	public string triggerAtaque = "ataque"; 
	public UnityEvent golpe;

	public Animator anim;

	// Use this for initialization
	void Start () {
		if(!anim){
			anim = GetComponent<Animator>();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)){
			atacar();
		}
	}

	public void atacar(){
		if(anim){
			anim.SetTrigger(triggerAtaque);
		}
		golpe.Invoke();
	}
}
