﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Golpeado : MonoBehaviour {
	public UnityEvent eventoGolpeado;

	void OnTriggerEnter(Collider col){
		
		if(col.transform.tag == "Finish"){
			golpeado();
		}
	}

	public void golpeado(){
		eventoGolpeado.Invoke();
	}
}
